package com.example.menu.Adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.menu.Model.Clinica;
import com.example.menu.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import util.NetworkUtils;

public class AdapterClin extends BaseAdapter {

    List<Clinica> clinicaList;
    Activity activity;

    public AdapterClin(List<Clinica> clinicaList, Activity activity) {
        this.clinicaList = clinicaList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return clinicaList.size();
    }

    @Override
    public Clinica getItem(int position) {
        return clinicaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.listview_activity, parent, false);
        Clinica clinica = getItem(position);

        ImageView imageView = view.findViewById(R.id.iv_image);
        String linkPhoto = "https://pouppe.websiteseguro.com/evosaude/sistema/fotos/"+clinica.getUniq_id()+'/'+clinica.getFoto();
        Picasso.with(view.getContext())
                .load(linkPhoto)
                .into(imageView);

        TextView nomeDaClinica = view.findViewById(R.id.tv_Clinica);
        nomeDaClinica.setText(clinica.getNome_fantasia());

        return view;
    }
}
