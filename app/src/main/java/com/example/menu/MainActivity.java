package com.example.menu;

import android.nfc.Tag;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.menu.Adapter.AdapterClin;
import com.example.menu.Model.Clinica;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import util.NetworkUtils;

public class MainActivity extends AppCompatActivity {

    TextView textoinicial;
    ProgressBar progressBar;
    final String TAG = "MainActivity";
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.pb_loading);
        textoinicial = findViewById(R.id.tv_texto_inicial);
        listView = findViewById(R.id.lv_lista);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_call_web:
                callWebService();
                break;
            case R.id.menu_clear:
                clearText();
                break;
            case R.id.menu_EVO:
                callWebServiceEvo();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void callWebService() {
        Log.d(TAG, "method callWebService");
        URL url = NetworkUtils.buildUrl("stf");
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void clearText() {
        Log.d(TAG, "method clearText");
        textoinicial.setText("");
    }

    public void callWebServiceEvo() {
        URL url = NetworkUtils.buildUrlCredenciadasEvo();
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void mostrarLoading() {
        textoinicial.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void esconderLoading() {
        textoinicial.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    public ListView listaClinica (List<Clinica> clinica){
        AdapterClin adapter = new AdapterClin(clinica, this);
        listView.setAdapter(adapter);
        return listView;
    }

    class MinhaAsyncTask extends AsyncTask<URL, Void, List<Clinica>> {

        @Override
        protected List<Clinica> doInBackground(URL... urls) {
            Log.d(TAG, "URL utilizada: " + urls.toString());
            URL url = urls[0];
            String json = null;
            try {
                json = NetworkUtils.getResponseFromHttpUrl(url);
                Log.d(TAG, "AsyncTask retornou: " + json);
            } catch (IOException e) {
                e.printStackTrace();
            }
            TypeToken<List<Clinica>> token = new TypeToken<List<Clinica>>(){};
            List<Clinica> clinicas = new Gson().fromJson(json.toString(), token.getType());

            return clinicas;
        }

        @Override
        protected void onPreExecute() {
            mostrarLoading();
        }

        @Override
        protected void onPostExecute(List<Clinica> s) {
            esconderLoading();
            if (s == null) {
                textoinicial.setText("Ocorreu um erro");
            } else {
                listaClinica(s);
            }


        }
    }
}