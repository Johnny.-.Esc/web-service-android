package com.example.menu.Model;

public class Clinica {

    private String descricao;
    private String foto;
    private String uniq_id;
    private String id_estabelecimento;
    private String razao_social;
    private String nome_fantasia;
    private String cnpj_cpf;
    private String contato;
    private String telefone;
    private String celular;
    private String email;
    private String endereco;
    private String numero;
    private String bairro;
    private String id_cidade;
    private String id_estado;
    private String cep;
    private String nmcidade;
    private String nmestado;
    private String pais;
    private String status;
    private String segmento;
    private String beneficios;
    private String total_likes;


    public String getDescricao() {
        return descricao;
    }

    public String getFoto() {
        return foto;
    }

    public String getUniq_id() {
        return uniq_id;
    }

    public String getId_estabelecimento() {
        return id_estabelecimento;
    }

    public String getRazao_social() {
        return razao_social;
    }

    public String getNome_fantasia() {
        return nome_fantasia;
    }

    public String getCnpj_cpf() {
        return cnpj_cpf;
    }

    public String getContato() {
        return contato;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCelular() {
        return celular;
    }

    public String getEmail() {
        return email;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getNumero() {
        return numero;
    }

    public String getBairro() {
        return bairro;
    }

    public String getId_cidade() {
        return id_cidade;
    }

    public String getId_estado() {
        return id_estado;
    }

    public String getCep() {
        return cep;
    }

    public String getNmcidade() {
        return nmcidade;
    }

    public String getNmestado() {
        return nmestado;
    }

    public String getPais() {
        return pais;
    }

    public String getStatus() {
        return status;
    }

    public String getSegmento() {
        return segmento;
    }

    public String getBeneficios() {
        return beneficios;
    }

    public String getTotal_likes() {
        return total_likes;
    }

    @Override
    public String toString() {
        return " nome_fantasia=" + nome_fantasia + "\n";
    }


}
